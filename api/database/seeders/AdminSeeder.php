<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (User::where('email', 'admin@test.test')->doesntExist()) {
            User::create([
                'email' => 'admin@test.test',
                'password' => bcrypt('admin'),
                'delete_status' => 0
            ]);
        }
    }
        
}
