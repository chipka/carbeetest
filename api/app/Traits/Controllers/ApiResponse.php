<?php


namespace App\Traits\Controllers;


use Illuminate\Http\JsonResponse;

trait ApiResponse
{
    /**
     * @param $data
     * @param $code
     * @return JsonResponse
     */
    public function respond($data, $code): JsonResponse
    {
        return response()->json($data, $code);
    }


    /**
     * @param       $message
     * @param int   $code
     * @param array $extra
     *
     * @return JsonResponse
     */
    public function respondWithError($message, $code = 400, array $extra = []): JsonResponse
    {
        $data = array_merge([
            'success' => false,
            'message' => $message
        ], $extra);

        return $this->respond($data, $code);
    }


    /**
     * @param $message
     * @param int $code
     * @return JsonResponse
     */
    public function respondWithSuccess($message, $code = 200): JsonResponse
    {
        return $this->respond([
            'success' => true,
            'message' => $message
        ], $code);
    }

    /**
     * @param $data
     * @return JsonResponse
     */
    public function respondWithData($data): JsonResponse
    {
        return $this->respond([
            'success' => true,
            'data'    => $data
        ], 200);
    }
}
