<?php

namespace App\Http\Controllers;

use App\Models\Car;
use App\Traits\Controllers\ApiResponse;
use Illuminate\Http\Request;

class CarController extends Controller
{
    use ApiResponse;

    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function getCars()
    {
        return $this->respondWithData(Car::all());
    }
}
