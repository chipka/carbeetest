<?php

namespace App\Http\Controllers;

use App\Models\Order;
use Illuminate\Http\Request;
use App\Traits\Controllers\ApiResponse;

class OrderController extends Controller
{
    use ApiResponse;

    public function getOrders(Request $request)
    {
        $yearStartFilter = $request->year_start;
        $yearEndFilter = $request->year_end;
        $makeFilter = $request->make;
        
        $data = Order::select('*');

        if ($yearStartFilter && $yearEndFilter) {
            $data->where('year', '>=', $yearStartFilter)
                ->where('year', '<=', $yearEndFilter);
        }

        if ($makeFilter) {
            $data->where('make', $makeFilter);
        }
        
        return $this->respondWithData($data->get());
    }
}
