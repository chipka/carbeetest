<?php

namespace App\Http\Controllers;

use App\Traits\Controllers\ApiResponse;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    use ApiResponse;
    /**
     * Login user
     * @param Request $request
     * @return JsonResponse
     */
    public function login(Request $request): JsonResponse
    {
        $company = null;
        $rules = [
            'email'    => ['required', 'string', 'email'],
            'password' => ['required', 'string'],
        ];
        $validator = Validator::make($request->all(), $rules);

        $credentials = [
            'email' => $request->email,
            'password' => $request->password
        ];

        if (! auth()->attempt($credentials)) {
            return $this->respondWithError(['The email or password is incorrect'], 401);
        }

        $token = auth()->user()->createToken(config('app.name'));
        $token->token->expires_at = Carbon::now()->addMonth();
        $token->token->save();

        return $this->respondWithData(['token' => $token->accessToken]);
    }
}
