import VueRouter from 'vue-router'
import Login from './components/Login.vue'
import Home from './components/Home.vue'
import Cars from './components/Cars.vue'
import Orders from './components/Orders.vue'
import { store } from './store'

const routes = [
    {path: '/login', component: Login, name: 'Login'},
    {path: '/home', component: Home, name: 'Home'},
    {path: '/cars', component: Cars, name: 'Cars'},
    {path: '/orders', component: Orders, name: 'Orders'},
];
const router = new VueRouter({
    routes,
    mode: 'history'
});

router.beforeEach((to, from, next) => {
    if (to.name !== 'Login' && !store.getters.getToken) next({ name: 'Login' })
    else if (to.name == 'Login' && store.getters.getToken) next({name: 'Home'})
    else next()
  })

export {router};